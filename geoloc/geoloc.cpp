#define _USE_MATH_DEFINES
#include <cmath>
#include "geoloc.hpp"

const double R = 6371e3 ;
double Distance(double lat1, double long1, double lat2, double long2) {
    double phi1 = lat1 * M_PI / 180.0 ;
    double phi2 = lat2 * M_PI / 180.0 ;
    double lambda1 = long1 * M_PI / 180.0 ;
    double lambda2 = long2 * M_PI / 180.0 ;

    double phidelta = phi2 - phi1 ;
    double lambdadelta = lambda2 - lambda1 ;
    double a = pow(sin(phidelta/2.0),2.0) + pow(sin(lambdadelta/2.0) , 2.0) * cos(phi1) * cos(phi2) ;
    double c = 2.0 * atan2( sqrt(a) , sqrt(1.0-a) ) ;
    return R * c ;
}