#ifndef _GEOLOC_HPP_
#define _GEOLOC_HPP_

// Reference: https://www.movable-type.co.uk/scripts/latlong.html

extern
double Distance(double lat1, double long1, double lat2, double long2) ; 

#endif