#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>
#include "geoloc.hpp"
using namespace std ;

// Reference: https://www.movable-type.co.uk/scripts/latlong.html
//            Test results verified with the app in the website above

void test_dist(double lat1, double long1 , double lat2, double long2) {
    double d ;
    d = Distance( lat1, long1 , lat2 , long2 ) ;
    cout << "lat1 " << lat1 << " long1 " << long1 ;
    cout << " lat2 " << lat2 << " long2 " << long2 ;
    cout << " distance " << d/1000.0 << " Kilometers " << endl ; 
}

int main(int argc, char **argv) {
    
    //lat1 1 long1 1 lat2 1 long2 1 distance 0 Kilometers 
    //lat1 1 long1 -1 lat2 1 long2 1 distance 222.356 Kilometers 
    //lat1 1 long1 1 lat2 -1 long2 1 distance 222.39 Kilometers 
    //lat1 1 long1 1 lat2 1 long2 -1 distance 222.356 Kilometers 


    test_dist( 1.0, 1.0, 1.0, 1.0 );
    test_dist( 1.0, -1.0, 1.0, 1.0 );  
    test_dist( 1.0, 1.0, -1.0, 1.0 );
    test_dist( 1.0, 1.0, 1.0, -1.0 );


    //lat1 55.5555 long1 55.5555 lat2 55.5555 long2 55.5555 distance 0 Kilometers 
    //lat1 55.5555 long1 -55.5555 lat2 55.5555 long2 55.5555 distance 6183.23 Kilometers 
    //lat1 55.5555 long1 55.5555 lat2 -55.5555 long2 55.5555 distance 12355 Kilometers 
    //lat1 55.5555 long1 55.5555 lat2 55.5555 long2 -55.5555 distance 6183.23 Kilometers 

    test_dist( 55.55555 , 55.55555, 55.55555, 55.55555 );
    test_dist( 55.55555 , -55.55555, 55.55555, 55.55555 );
    test_dist( 55.55555 , 55.55555, -55.55555, 55.55555 );
    test_dist( 55.55555 , 55.55555, 55.55555, -55.55555 );
}