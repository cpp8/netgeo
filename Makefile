CXX=clang
EXEC=netgeo
include Makefile.std
SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.hpp) 
OBJECTS = $(SOURCES:.cpp=.o)
UNITTESTS = $(wildcard *_test.cpp)
UNITTESTOBJECTS = $(UNITTESTS:.cpp=.o)

OBJLIB = ./lib/libnetgeo.a 
LIBOBJECTS := $(filter-out $(UNITTESTOBJECTS),$(OBJECTS))


all:  $(HEADERS) $(OBJLIB) $(OBJECTS)
	$(CXX) $(OBJECTS) $(CXXFLAGS) $(LDFLAGS) $(OBJLIB) -o $(EXEC)

clean:
	rm -f $(EXEC) $(OBJECTS)
	rm -rf $(OBJLIB)
	
$(OBJLIB) : 
	$(MAKE) -C options clean all
	$(MAKE) -C discovery clean all
	$(MAKE) -C geoloc clean all
	$(MAKE) -C ipquery clean all

%.o:%.cpp
	$(CXX) $(CXXFLAGS) -c $(<)