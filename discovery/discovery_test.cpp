#include <iostream>
#include "discovery.hpp"
using namespace std ;

void discover_node(int argc, char** argv) {
        for (int ptr=1; ptr < argc; ptr++) {
        Discovery discovery ;
        Nodes nodes ;
        discovery.Discover( argv[ptr] , nodes );
        discovery.Show(nodes) ;
    }
}

void line_tests(string line) {
    Nodes nodes ;
    Discovery discovery ;
    discovery.Extract(line,nodes) ;
    discovery.Show(nodes) ;
}

void line_tests() {
    line_tests("* * *");
    line_tests("192.168.200.1  3.061 ms  3.355 ms  3.995 ms") ;
    line_tests("140.222.0.223  8.509 ms 140.222.0.221  7.121 ms  6.865 ms") ;
    line_tests("63.65.76.62  12.970 ms  10.453 ms  12.370 ms");
}

int main(int argc, char** argv) {
    cout << "Discovery Test" << endl ;
    line_tests() ;
    discover_node( argc, argv) ;

}