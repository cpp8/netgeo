#ifndef _DISCOVERY_HPP_
#define _DISCOVERY_HPP_
#include <string>
#include <vector>
#include <glib.h>

typedef std::vector<std::string> Nodes ;

class Discovery {
    public:
        Discovery();
        void Discover(std::string _target, Nodes& _nodes) ;
        void Extract(std::string _line, Nodes& _nodes) ;
        void Show(Nodes& _nodes) const ;
        static bool Verbose ;
    private:
        static GRegex* ipregex ;
} ;

#endif
