#include <iostream>
#include <algorithm>
#include "discovery.hpp"
using namespace std ;

static const string command = "traceroute -n " ;
static const string regex = "([0-9]{1,3}\\.){3}[0-9]{1,3}" ;

bool Discovery::Verbose = false ;
GRegex* Discovery::ipregex=NULL ;

Discovery::Discovery() {
    if (ipregex == NULL) {
        GError *error = NULL ;
        ipregex = g_regex_new( regex.c_str() , GRegexCompileFlags(0) ,GRegexMatchFlags(0) , &error ) ;
        if (ipregex == NULL) {
            cerr << "Regex compilation error" << endl ;
            cerr << error->message << endl ;
        } else {
            if (Verbose) cerr << "Regex compiled" << endl ;
        }
    }
}
void Discovery::Discover(std::string _target, Nodes& _nodes) {

    string fullcmd = command + " " + _target ;
    if (Verbose) cout << "Executing " << fullcmd << endl ;
    GError *error =NULL;
    gchar *stderr =NULL;
    gchar *stdout =NULL;
    gboolean status = false ;
    gint exit_status = 0 ;
    status = g_spawn_command_line_sync( fullcmd.c_str() , &stderr, &stdout, &exit_status, &error ) ;
    if (status) {
        if (Verbose) cout << stdout << endl ;
         if (Verbose) cout << stderr << endl ;
    } else {
        cerr << "spawn failed" << endl ;
    }
    Extract(stdout, _nodes );
    Extract(stderr, _nodes );   

}

void Discovery::Show(Nodes& _nodes) const {
     if (Verbose) cout << "Displaying nodes" << endl ;
    for (Nodes::iterator ptr=_nodes.begin(); ptr != _nodes.end(); ptr++) {
        cout << *ptr << endl ;
    }
}

void Discovery::Extract(std::string _line, Nodes& _nodes) {
    int count = 0 ;
    GMatchInfo *match_info = NULL ;
    g_regex_match( ipregex , _line.c_str() , GRegexMatchFlags(0) , &match_info) ;
    while (g_match_info_matches(match_info)) {
        gchar *word = g_match_info_fetch(match_info, 0);
        if (Verbose) 
            cout << "Found " << word << endl ;
        string ipadr (word) ;
        auto it = find(_nodes.begin(), _nodes.end(), word); 
        if (it == _nodes.end()) {
            if (Verbose) cout << "Not seen this before. Will save" << endl ;
            _nodes.push_back( ipadr ) ;
        }
        g_free(word) ;
        g_match_info_next(match_info,NULL);
        count++ ;
    }
    g_match_info_free(match_info) ;
    if (Verbose) cout << "Found " << count << " IP Addresses" << endl ;

}