# netgeo

Network Geography Exploration

## Usage
```
$ ./netgeo
netgeo - Version 0.1 - Sat Sep  5 10:31:12 2020
usage: netgeo <options> ipspec [ipspec...]
         -v --verbose    Verbose
         -h --help       Help
         -t --trace - with this option, a traceroute to the argument is done and the distances of each hop is shown
                      without the trace option, the distances of each of the command line arguments is shown
```

## Example trace
```

Note: We are not able to find the distance to the first hop due to variety of factors.

./netgeo --trace 23.40.30.30 
Decryption key set to : 2020-09-06 12:12:232020-09-06 12:12:23
192.168.200.1 : cannot get geography details
192.168.1.1 : cannot get geography details
             IP Address - from              IP Address - to       Distance in Kms
                   63.65.76.62                  140.222.0.223           38.001
                 140.222.0.223                  140.222.0.221                0
                 140.222.0.221                 130.81.221.114           38.001
                130.81.221.114                    23.40.30.30          130.215
```
## Discover some well known domains
```
Note: these addresses belonging to well known organizations may happen to be hosted clustered in the northwest US!

 ./netgeo 23.40.30.30 23.33.97.165 172.217.11.4 23.40.30.30
Decryption key set to : 2020-09-06 12:12:232020-09-06 12:12:23
             IP Address - from              IP Address - to       Distance in Kms
                   23.40.30.30                   172.217.11.4          130.215
                  172.217.11.4                   23.33.97.165                0
                  23.33.97.165                    23.40.30.30          130.215
```