#include <iostream>
#include <iomanip>
#include "options/options.hpp"
#include "ipquery/ipquery.hpp"
#include "geoloc/geoloc.hpp"
#include "discovery/discovery.hpp"

using namespace std ;


Nodes nodes;

bool AlreadySeen( Details::iterator _details, string ip) {
    if (_details->ip.compare(ip) == 0) return true ;
    return false ;
}
bool compare (string left, string right) {
    if (left.compare(right) == 0) return true ;
    return false ;
}
int main(int argc, char **argv) {
    Discovery discovery ;
    Options options(argc,argv) ;
    int argptr = options.ArgPtr() ;
    discovery.Verbose = options.Verbose ;

    if (options.Trace) {
        if (argptr < argc) {
            discovery.Discover( argv[argptr] , nodes ) ;
            //nodes.pop_front() ;
        }
    } else {
        for (int arg=argptr; arg < argc; arg++) {
            nodes.push_back( argv[arg]) ;
        }
    }
    if (options.Verbose) discovery.Show(nodes) ;
    Nodes::iterator nodeptr; // = nodes.begin() ;
    Details details ;
    for (nodeptr=nodes.begin() ; nodeptr != nodes.end() ; nodeptr++) {
        string newnode(*nodeptr) ;
        IPQuery ipquery(*nodeptr) ;
        ipquery.Verbose = options.Verbose ;
        if (!ipquery.Valid()) {
            cerr << newnode << " : cannot get geography details" << endl ;
        } else {
            details.push_front( ipquery.Details ) ;
            if (options.Verbose) cout << "IP " << ipquery.Details.ip << " latitude " << ipquery.Latitude() << " longitude " << ipquery.Longitude() << endl ;
        }
    } 

    Details::iterator detptr = details.begin() ;
    Details::iterator firstptr = detptr++;

    double Distance(double lat1, double long1, double lat2, double long2) ; 
    cout << setw(30) << "IP Address - from" << setw(30) << "IP Address - to " << setw(16) << "      Distance in Kms" << endl ;
    for (Details::iterator ptr = detptr ; ptr != details.end(); ptr++) {
        double dist = Distance( firstptr->latitude , firstptr->longitude , 
                                ptr->latitude      , ptr->longitude ) ;
        cout << setw(30) << firstptr->ip << " " << setw(30)<< ptr->ip << " " << setw(16) << dist/1000.0 << endl;
        firstptr = ptr ;
    }
}