#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include "options.hpp"

using namespace std ;

Options::Options(int _argc, char **_argv) 
: argc(_argc) , argv(_argv) , argptr(0)
{
    if (argc <= 1) {
        Help() ;
        exit(EXIT_FAILURE) ;
    }
    Verbose = false ;
    Trace = false ;

}

void Options::Help() {
    cerr << "netgeo - Version 0.1 - " << __TIMESTAMP__ << endl ;
    cerr << "usage: netgeo <options> ipspec [ipspec...]" << endl ;
    cerr << "\t -v --verbose \t Verbose" << endl ;
    cerr << "\t -h --help\t Help" << endl ;
    cerr << "\t -t --trace - with this option, a traceroute to the argument is done and the distances of each hop is shown" << endl ;
    cerr << "\t              without the trace option, the distances of each of the command line arguments is shown" << endl ;
    exit(EXIT_FAILURE);
}

int Options::ArgPtr() {
    int opt ;
    int idxptr = 0 ;

    if (optind > 1) {
        return optind ;
    }

    struct option options [] = {

        {"help" , 0 , 0 , 'h'} ,
        {"trace" , 0 , 0 , 't' } ,
        {"verbose" , 0 , 0 , 'v'} ,
        {0 , 0, 0, 0}
    } ;

    while ((opt = getopt_long(argc, argv, "htv" , options, &idxptr )) != -1) {
        switch (opt) {
        case 'h':
            Help() ;
            continue ;
        case 'v':
            Verbose = true ;
            cerr << "Verbose" << endl ;
            continue ;
        case 't' :
            Trace = true ;
            if (Verbose) {
                cerr << "Trace option. will execute traceroute to cli arg" << endl ;
            }
            continue ;
        default: 
            Help();
            break;
        }
    }
    return optind ;    
}