#include <iostream>
#include <cmath>
#include <cfloat>
#include <jansson.h>
#include "ipquery.hpp"

using namespace std ;

bool IPQuery::parseDetails() {
    json_t *json;
    json_error_t error;
    char *formatted ;
    json = json_loads(query_results.c_str(),JSON_DECODE_ANY, &error) ;
    if (json == NULL) {
        cerr << "Error Parsing " << query_results << endl ;
        return false ;
    } else {
        formatted = json_dumps(json,JSON_INDENT(4)) ;
        if (Verbose) cout << "Parsed " << formatted << endl ;
        free(formatted) ;
    }
    
    const char *str ;
    json_unpack(json, "{s:s}" , "ip" , &str ) ;
    Details.ip = str ;

    json_unpack(json, "{s:s}" , "country_name" , &str ) ;
    Details.country_name = str ;
    json_unpack(json, "{s:s}" , "region_name" , &str ) ;
    Details.region_name = str ;
    json_unpack(json, "{s:s}" , "region_code" , &str ) ;
    Details.region_code = str ;

    json_unpack(json, "{s:s}" , "city" , &str ) ;
    Details.city = str ;


    double lat,lot ;
    json_unpack(json, "{s:f}" , "latitude" , &lat ) ;
    json_unpack(json, "{s:f}" , "longitude" , &lot) ;
    if (std::isnan(lat) || std::isnan(lot)) return false ;
    Details.latitude = lat ;
    Details.longitude = lot ;

    return true ;
}