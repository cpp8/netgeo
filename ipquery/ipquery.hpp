#ifndef _IPQUERY_HPP_
#define _IPQUERY_HPP_

#include <string>
#include <vector>
#include <list>
#include <curl/curl.h>

// Reference: IP to geolocation API provided by
//            https://ipstack.com/

typedef enum {
    IPV4, 
    IPV6
} IPAddressType ;

typedef struct Language {
    std::string code ;
    std::string name ;
    std::string native ;
} Language ;

typedef struct {
    unsigned long geoname_id ; 
    std::string capital ;
    std::vector<Language> languages ;
    std::string country_flag ; 
    std::string country_flag_emoji ; 
    std::string country_flag_emoji_unicode ;
    std::string calling_code ; 
    bool is_eu ;
} Location ;

typedef struct {
    std:: string ip; 
    IPAddressType type ;
    std::string continent_code ;
    std::string continent_name ;
    std::string country_code ;
    std::string country_name ;
    std::string region_code ;
    std::string region_name ; 
    std::string city ; 
    std::string zip ; 
    //std::string latitude ; 
    //std::string longitude ; 
    double latitude ;
    double longitude ;
    Location location ;
} GeoDetails ;

typedef std::list<GeoDetails> Details ;

class IPQuery {
    public:
        IPQuery() ;
        IPQuery(std::string _ipref) ;
        ~IPQuery() ;
        bool Valid() const { return valid ; }
        bool parseDetails() ;
        double Latitude() const throw() ;
        double Longitude() const throw() ;
        
        std::string Location() const throw() ;
        GeoDetails Details ;
        static bool Verbose ;
        
    private:

        static bool initialized ;
        static std::string url ;
        static std::string key ;
        static std::string params ;

        static CURL *curl ;


        static bool Initialize() ;

        std::string ipref ;
        std::string query_results ;
        bool valid ;        

        static size_t CaptureResult(void *contents, size_t size, size_t nmemb, void *userp) ;
} ;

#endif
