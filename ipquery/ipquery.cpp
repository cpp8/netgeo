#include <iostream>
#include <resourcelib.hpp>
#include <resourcelib.cpp>
#include "resources.h"

#include "ipquery.hpp"
using namespace std ;
bool IPQuery::initialized = false ;
std::string IPQuery::url = "http://api.ipstack.com/" ;
std::string IPQuery::key = "" ;
CURL* IPQuery::curl = NULL ;
bool IPQuery::Verbose = false ;

IPQuery::IPQuery() 
: valid(false)
{
   if (Verbose) cout << "IPQuery: Default constructor" << endl ;
}

IPQuery::IPQuery(std::string _ipref) 
: IPQuery()
{
    if (!initialized) {
        initialized = Initialize() ;
    }
    ipref = _ipref ;

    string params = "access_key=" ; 
    params.append(key) ;

    string fullurl = url + _ipref + "?" + params ;
    
    //curl_easy_setopt(curl,CURLOPT_URL,fullurl) ;
    if (Verbose) cout << "Full URL : " << fullurl << endl ;

    CURLU *urlp = curl_url() ;
    curl_url_set(urlp, CURLUPART_URL , fullurl.c_str() , 0 );
    curl_easy_setopt(curl,CURLOPT_CURLU, urlp) ;
    curl_easy_setopt(curl,CURLOPT_HTTPGET,1L) ;
 
    curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION, &IPQuery::CaptureResult );
    curl_easy_setopt(curl,CURLOPT_WRITEDATA, &query_results );
    auto res = curl_easy_perform(curl);
    if(res != CURLE_OK) {
        cerr << "curl_easy_perform() failed:\n" << curl_easy_strerror(res) << endl ;
        return ;
    }
    curl_url_cleanup( curl_url() ) ;

    //cout << "Result is " << query_results << endl ;
    valid = parseDetails() ;
}
bool IPQuery::Initialize() {
    curl = curl_easy_init();
    if (curl == NULL) {
        throw "Failed curl_easy_init" ;
    }

    std::byte* apikey ;
    apikey = Retrieve(ID_APIKEY, FILENAME_APIKEY, FILESIZE_APIKEY, MD5_HASH_APIKEY, DATA_APIKEY, true, BLOCK_SIZE, CREATION_TIMESTAMP ) ;
    if (apikey == NULL) {
        cerr << "Failure to load API key" << endl ;
        throw "APIKEY" ;
    }
    if (Verbose) cout << "APIKEY loaded " << (char *)apikey << endl ;
    key="" ;
    char *kptr ;
    for (kptr=(char *)apikey ; *kptr != 0 && *kptr != '"' ; kptr++) {
    }
    char keychar[2] ; keychar[1] = 0 ;
    for (++kptr ; *kptr != 0 && *kptr != '"' ; kptr++) {
        keychar[0] = *kptr ;
        key.append( keychar );
    }
    return true ;
}

IPQuery::~IPQuery() {
    if (Verbose)  cout << "IPQuery: destructor" << endl ;
}

double IPQuery::Latitude() const throw() {
   if (!valid) {
       throw "Invalid IP address" ;
   }
   return Details.latitude ;
}

double IPQuery::Longitude() const throw() {
   if (!valid) {
       throw "Invalid IP address" ;
   }
   return Details.longitude ;
}

std::string IPQuery::Location() const throw() {
   if (!valid) {
       throw "Invalid IP address" ;
   }
   return Details.city + ", " + Details.region_code + " " + Details.country_name ;
}

size_t IPQuery::CaptureResult(void *contents, size_t size, size_t nmemb, void *userp) {
    ((std::string *)userp)->append((char *)contents, size * nmemb) ;
    return size * nmemb ;
}