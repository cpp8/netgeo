#include <iostream>
#include <iomanip>
#include <string>
#include "ipquery.hpp"

using namespace std ;
//std::string url = "www.amazon.com" ;
//std::string host="99.84.239.83" ;
std::string host ;

int main(int argc, char **argv) {
    host = argv[1] ;
    IPQuery ipquery (host) ;
    
    cout << "Queried about " << host << endl ;
    if (ipquery.Valid()) {
        cout << "Latitude " <<  std::setprecision(12) << ipquery.Latitude() << " Longitude "  << ipquery.Longitude() << endl ;
        cout << "Location " << ipquery.Location() << endl ;
    } else {
        cout << "Query failed" << endl ;
    }
    return EXIT_SUCCESS ;
}